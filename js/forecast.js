const API_KEY = "18cbce0c936b81afc7e8189a8357e16c";
const BASE_URL = "https://api.openweathermap.org/data/2.5/forecast";

const getForecast = async (city) => {
    let requestURL = BASE_URL + `?q=${city}&units=metric&appid=${API_KEY}`;

    const response = await fetch(requestURL);

    if(response.ok) {
        const data = await response.json();
        return data;
    } else {
        throw new Error("Status Code: " + response.status);
    }

}

getForecast("Mumbai")
    .then(data => {
        console.log(data);
    })
    .catch(err => {
        console.error(err);
    })